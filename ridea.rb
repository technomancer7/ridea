#!/usr/local/bin/ruby
$LOAD_PATH.unshift "#{Dir.home}/.ridea.d"
$LOAD_PATH.unshift "#{Dir.home}/.ridea.d/autoload"
require "gtk2"
require "gtksourceview2"
require 'json'
require 'mimemagic'
require 'opt'
require 'libkaiser'
require 'gtkp'

class Ridea
  
  def writeConfig
    File.open("#{Dir.home}/.ridea.d/config.json", "w+") { |file|
      file.write(@config.to_json)
    }
  end
  def changeLang(lang)
    n_lang = Gtk::SourceLanguageManager.new.get_language(lang)
    @cbox.buffer.language = n_lang
  end

  def buffer(i = 0)
    @cbox.buffer = @buffers[i][:buffer]
    @buffer = i
    @root_window.title = "#{@buffers[i][:file]} (#{i}/#{@buffers.length-1})"
  end
  
  def newBuffer(filepath, txt = nil)
    f = {buffer: Gtk::SourceBuffer.new, file: filepath}
    if txt != nil
      f[:buffer].text = txt
    end
    @buffers.append f
    f
  end

  def closeBuffer(b = -1)
    if b <= 0 or b >= @buffers.length
      b = @buffer
    end

    if b == @buffer
      prevBuffer
    end
    @buffers.delete_at(b)
    @root_window.title = "#{@buffers[@buffer][:file]} (#{@buffer}/#{@buffers.length-1})"
  end
  def lastBuffer
    buffer(@buffers.length - 1)
  end

  def nextBuffer
    @buffer += 1
    if @buffer >= @buffers.length
      @buffer = 0
    end
    buffer(@buffer)
  end
  def prevBuffer
    @buffer -= 1
    if @buffer < 0
      @buffer = @buffers.length - 1
    end
    buffer(@buffer)
  end  
  def initialize
    @opt = OptHandler.new
    @dir = "#{Dir.home}/.ridea.d/"
    @ext_dir = "#{Dir.home}/.ridea.d/autoload/"
    if not File.exist?(@dir); Dir.mkdir(@dir); end
    if not File.exist?(@ext_dir); Dir.mkdir(@ext_dir); end
    
    if not File.exist?("#{@dir}config.json")
      File.open("#{@dir}config.json", "w+") { |file| file.write("{}") }
      @config = {}
      @config["theme"] = {}
      @config["theme"]["main_txt_font"] = "Monospace 9"
      @config["internals"] = {}
      @config["internals"]["blacklist"] = []
      @config["internals"]["autoloads"] = []
      writeConfig
    else
      @config = JSON.parse(File.read("#{@dir}config.json"))
    end
    @extensions = {}
    @buffers = []
    @buffer = 0
    @root_window = Gtk::Window.new
    @root_window.set_size_request(450, 500)
    @root_window.title = "New File"
    @layout = Gtk::Table.new 6, 6, false
    @root_window.add @layout
    @codes = Gtk::ScrolledWindow.new
    @root_window.signal_connect('destroy') { Gtk.main_quit }
    @cbox = Gtk::SourceView.new
    @cbox.show_line_marks = true
    @cbox.show_line_numbers = true
    @cbox.wrap_mode = Gtk::TextTag::WRAP_WORD
    @codes.set_policy(Gtk::POLICY_AUTOMATIC, Gtk::POLICY_AUTOMATIC)
    @buffers.append( {buffer: @cbox.buffer, file: nil} )
    @layout.attach @codes, 0, 6, 1, 6, Gtk::EXPAND|Gtk::FILL, Gtk::EXPAND|Gtk::FILL, 1, 1
    @codes.add @cbox
    
    font = Pango::FontDescription.new("Monospace 9")
    @cbox.modify_font(font)

    
    menubar = Gtk::MenuBar.new
    file = Gtk::MenuItem.new("File")
    edit = Gtk::MenuItem.new("Edit")
    bufs = Gtk::MenuItem.new("Buffers")
    ext = Gtk::MenuItem.new("Extend")
    help = Gtk::MenuItem.new("Help")
    filemenu = Gtk::Menu.new
    editmenu = Gtk::Menu.new
    extmenu = Gtk::Menu.new
    helpmenu = Gtk::Menu.new
    bufmenu = Gtk::Menu.new
    file.submenu = filemenu
    edit.submenu = editmenu
    ext.submenu = extmenu
    bufs.submenu = bufmenu
    help.submenu = helpmenu
    menubar.append(file)
    menubar.append(edit)
    menubar.append(bufs)
    menubar.append(ext)
    menubar.append(help)
    
    group = Gtk::AccelGroup.new
    new = Gtk::ImageMenuItem.new(Gtk::Stock::NEW, group)
    open = Gtk::ImageMenuItem.new(Gtk::Stock::OPEN, group)
    save = Gtk::ImageMenuItem.new(Gtk::Stock::SAVE, group)
    saveas = Gtk::MenuItem.new("Save As")
    new.signal_connect('activate') { |w| puts "w=#{w.class}:New selected" }
    open.signal_connect('activate') { |w| promptOpenFile }
    save.signal_connect('activate') { |w|
      f = @buffers[@buffer][:file]
      if f == nil
        promptSaveFileAs
      else
        saveFile f
      end
    }
    saveas.signal_connect('activate') { |w| promptSaveFileAs }
    filemenu.append(new)
    filemenu.append(open)
    filemenu.append(save)
    filemenu.append(saveas)
    changecodelang = Gtk::MenuItem.new("Change Language")
    changecodelang.signal_connect('activate') { |w|
      @langc = Gtk::Window.new "Language"
      @lanhb = Gtk::HBox.new
      @langc.add @lanhb

      @langen = Gtk::Entry.new
      @langb = Gtk::Button.new "update"
      @langb.signal_connect('clicked') {
        changeLang @langen.text
        @langc.destroy
      }
          
      @lanhb.add @langen
      @lanhb.add @langb
      @langc.show_all
    }
    editmenu.append changecodelang
    
    setv = Gtk::MenuItem.new("Settings")
    setv.signal_connect('activate') { |w|
      f = HashEditor.new @config
      f.callback { |jso| 
        @config = jso
        writeConfig
      }
    }
    editmenu.append setv

    extb = Gtk::MenuItem.new("Extensions")
    extb.signal_connect('activate') { |w| openExtensionManager }
    extmenu.append extb

    extev = Gtk::MenuItem.new("Live Console")
    extev.signal_connect('activate') { |w| openEval }
    extmenu.append extev

    
    pb = Gtk::MenuItem.new("Previous")
    nb = Gtk::MenuItem.new("Next")
    cb = Gtk::MenuItem.new("Close")
    nb.signal_connect('activate') { |w| nextBuffer}
    pb.signal_connect('activate') { |w| prevBuffer}
    cb.signal_connect('activate') { |w| closeBuffer}
    bufmenu.append nb
    bufmenu.append pb
    bufmenu.append cb
    vbox = Gtk::VBox.new(homogeneous = false, spacing = nil)
    vbox.pack_start_defaults(menubar)
    #vbox.pack_start_defaults(label)
    @layout.attach vbox, 0, 6, 0, 1, Gtk::EXPAND|Gtk::FILL, Gtk::SHRINK
    
    @root_window.show_all
    loadExtensions
    Gtk.main
  end
  def tag(textview, search, tag)
    eob_mark = textview.buffer.create_mark(nil,textview.buffer.start_iter.forward_to_end,false)
    textview.scroll_mark_onscreen(eob_mark)

    start = textview.buffer.start_iter
    r = start.forward_search(search, Gtk::TextIter::SEARCH_TEXT_ONLY, nil)

    while (r)
      start.forward_char
      r = start.forward_search(search, Gtk::TextIter::SEARCH_TEXT_ONLY, nil)
      break if r == nil
      start = r[0]
      textview.buffer.apply_tag(tag, r[0], r[1])
    end
  end
  def openFile(path)
    puts path
    data = File.read(path)
    b = newBuffer(path, data)
    lastBuffer
    puts @buffers
    f = MimeMagic.by_path(path).type
    changeLang f
  end
  
  def saveFile(path)
    puts path
    File.open(path, "w+") {
      |file| file.write "#{@cbox.buffer.text}";
    };
    @buffers[@buffer][:file] = path
  end
  
  def promptSaveFileAs
    dialog = Gtk::FileChooserDialog.new("Save File", @root_window, Gtk::FileChooser::ACTION_SAVE, nil,
                                        [Gtk::Stock::CANCEL, Gtk::Dialog::RESPONSE_CANCEL],
                                        [Gtk::Stock::OPEN, Gtk::Dialog::RESPONSE_ACCEPT])
    if dialog.run == Gtk::Dialog::RESPONSE_ACCEPT; saveFile dialog.filename;end;dialog.destroy
  end
  
  def promptOpenFile
    dialog = Gtk::FileChooserDialog.new("Open File", @root_window, Gtk::FileChooser::ACTION_OPEN, nil,
                                        [Gtk::Stock::CANCEL, Gtk::Dialog::RESPONSE_CANCEL],
                                        [Gtk::Stock::OPEN, Gtk::Dialog::RESPONSE_ACCEPT])
    if dialog.run == Gtk::Dialog::RESPONSE_ACCEPT; openFile dialog.filename;end;dialog.destroy
  end

    def autoloadExtension(ext_id)
    @config["internals"]["autoloads"].append ext_id
    writeConfig
  end
  
  def unautoloadExtension(ext_id)
    @config["internals"]["autoloads"].delete ext_id
    writeConfig
  end
  
  def blacklistExtension(ext_id)
    @config["internals"]["blacklist"].append ext_id
    writeConfig
  end
  
  def unblacklistExtension(ext_id)
    @config["internals"]["blacklist"].delete ext_id
    writeConfig
  end
  
  def setv(grp, key, value)
    unless @config[grp].key? key
      @config[grp] = {}
    end
    @config[grp][key] = value
    writeConfig
  end
  
  def getv(grp, key, default)
    @config[grp] = {} unless @config.key? grp
    if @config[grp].key? key
      return @config[grp][key]
    end
    default
  end
  
  def openExtensionManager
    @extwin = Gtk::Window.new
    @extwin.set_size_request(350, 150)
    @extlay = Gtk::ScrolledWindow.new
    @extlay.set_policy(Gtk::POLICY_AUTOMATIC, Gtk::POLICY_ALWAYS)
    @hb = Gtk::VBox.new
    @extlay.add_with_viewport @hb
    
    Dir.foreach("#{Dir.home}/.ridea.d/autoload/") do |entry|
      if entry.end_with? ".rb"
        e = Gtk::CheckButton.new(entry.split(".")[0])
        if @extensions[entry.split(".")[0]][:enabled]
          e.active = true
        end
        e.signal_connect( "toggled" ) { |w|
          if w.active? #turn off ext
            send "load_#{w.label}", self
            @extensions[entry.split(".")[0]][:enabled] = true
          else
            send "unload_#{w.label}", self
            @extensions[entry.split(".")[0]][:enabled] = false
          end
        }
        @hb.add e
      end
    end
    

    @extwin.add @extlay
    @extwin.show_all
  end

  def loadExtensions
    blacklist = []
    autos = []
    blacklist = @config["internals"]["blacklist"] unless @config["internals"]["blacklist"] == nil
    autos = @config["internals"]["autoloads"] unless @config["internals"]["autoloads"] == nil
    Dir.foreach("#{Dir.home}/.ridea.d/autoload/") do |entry|
      if entry.end_with? ".rb"
        puts "Loading extension #{entry} in to memory..."

        unless blacklist.include? entry.split(".")[0]
          @extensions[entry.split(".")[0]] = {ui_elements: {}, enabled: false}
          require entry.split(".")[0]

          if autos.include? entry.split(".")[0]
            send "load_#{entry.split(".")[0]}", self
            @extensions[entry.split(".")[0]][:enabled] = true
          end
        end
      end
    end
  end

  def openEval
    if @codewin != nil and @coutwin != nil
      @codewin.destroy
      @coutwin.destroy
    else
      @codewin = Gtk::Window.new
      @codes = Gtk::ScrolledWindow.new
      ruby_lang = Gtk::SourceLanguageManager.new.get_language('ruby')
      revalbuf = Gtk::SourceBuffer.new ruby_lang
      @cbox = Gtk::SourceView.new revalbuf
      @cbox.show_line_marks = true
      @cbox.show_line_numbers = true
      @cbox.wrap_mode = Gtk::TextTag::WRAP_WORD
      @codes.set_policy(Gtk::POLICY_AUTOMATIC, Gtk::POLICY_AUTOMATIC)
      @coutwin = Gtk::Window.new
      @couts = Gtk::ScrolledWindow.new
      @cout = Gtk::TextView.new
      @cout.wrap_mode = Gtk::TextTag::WRAP_WORD
      @couts.set_policy(Gtk::POLICY_AUTOMATIC, Gtk::POLICY_AUTOMATIC)
      @codewin.title = "Live Evaluation"
      @codewin.set_size_request(500, 450)
      @coutwin.set_size_request(450, 200)
      @codewin.signal_connect('destroy') { @codewin = nil }
      @code = Gtk::Table.new 5, 6, false
      @cout.buffer.create_tag("err",           {"underline"     => Pango::Underline::SINGLE, "foreground" => "#ff0000"})
      @coutwin.add @couts
      @couts.add @cout
      @coutwin.title = "Eval Output"
      @coutwin.show_all
      @codewin.add @codes
      @codes.add @cbox
      @codewin.show_all
      
      @cbox.signal_connect('key_press_event') { |w, e|
        if e.keyval == 65474
          begin
            result = eval @cbox.buffer.text
            @cout.buffer.text += "#{result}\n"
          rescue Exception => e  
            @cout.buffer.text += e.message+"\n"
            tag @cout, "#{e.message}", "err"
            for bt in e.backtrace
              @cout.buffer.text += "-- #{bt}\n"
            end
          end
          @cout.buffer.text += "---------------\n"
        end
      }
    end 
  end
end

if __FILE__ == $0
  Ridea.new
end

