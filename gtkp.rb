def choices(title, label, items, &fn)
  prompt = Gtk::Window.new
  prompt.title = title
  v = Gtk::VBox.new
  v.add Gtk::Label.new(label)

  returns = {}
  items.each do |item| 
    i = item.dup
    returns[i] = Gtk::Button.new i
    returns[i].signal_connect('clicked') {
      fn.call i
      prompt.destroy()
    }
    v.add returns[i]
  end
  prompt.add v
  prompt.show_all
end

def ask(title, label, &fn)
  prompt = Gtk::Window.new
  prompt.title = title
  v = Gtk::VBox.new
  v.add Gtk::Label.new(label)
  
  items = Gtk::HBox.new
  prompt_yes = Gtk::Button.new "Yes"
  prompt_no = Gtk::Button.new "No"

  v.add items
  items.add prompt_yes
  items.add prompt_no

  prompt.add v
  prompt_yes.signal_connect('clicked') {
    fn.call true
    prompt.destroy()
  }
  prompt_no.signal_connect('clicked') {
    fn.call false
    prompt.destroy()
  }
  prompt.show_all
end


def openPrompt(title, label, &fn)
  prompt = Gtk::Window.new
  prompt.title = title
  v = Gtk::VBox.new
  v.add Gtk::Label.new(label)
  
  items = Gtk::HBox.new
  prompt_entry = Gtk::Entry.new
  prompt_send = Gtk::Button.new "Confirm"

  v.add items
  items.add prompt_entry
  items.add prompt_send

  prompt.add v
  prompt_send.signal_connect('clicked') {
    fn.call prompt_entry.text
    prompt.destroy()
  }
  prompt.show_all
end

def openPromptDefault(title, label, default, &fn)
  prompt = Gtk::Window.new
  prompt.title = title
  v = Gtk::VBox.new
  v.add Gtk::Label.new(label)
  
  items = Gtk::HBox.new
  prompt_entry = Gtk::Entry.new
  prompt_entry.text = default
  prompt_send = Gtk::Button.new "Confirm"

  v.add items
  items.add prompt_entry
  items.add prompt_send

  prompt.add v
  prompt_send.signal_connect('clicked') {
    fn.call prompt_entry.text
    prompt.destroy()
  }
  prompt.show_all
end

def askDir(prompt = "Open directory", root=Dir.home, &fn)
  dialog = Gtk::FileChooserDialog.new(prompt, @root_window, Gtk::FileChooser::ACTION_SELECT_FOLDER, nil,
                                      [Gtk::Stock::CANCEL, Gtk::Dialog::RESPONSE_CANCEL],
                                      [Gtk::Stock::OPEN, Gtk::Dialog::RESPONSE_ACCEPT])
  dialog.current_folder = root
  if dialog.run == Gtk::Dialog::RESPONSE_ACCEPT;
    fn.call dialog.filename;
  end;
  dialog.destroy;
end


def addToTree(ls, string, base = nil); ls.model.append(base)[0] = string; end

class HashEditor
  def callback(&fn)
    @callback = fn
  end
  
  def initialize(data, title = "Editor")
    @data = data
    @window = Gtk::Window.new
    @window.title = title
    @lines = Gtk::VBox.new
    @controls = Gtk::HBox.new
    @add = Gtk::Button.new "+"
    @save = Gtk::Button.new "Save"
    @controls.add @add;@controls.add @save;
    @window.add @lines
    @lines.add @controls
    
    @linesc = {}
    
    data.each { |k, v|
      addRow k, v
    }

    @add.signal_connect('clicked') {
      openPrompt("New key", "Enter new key: ") { |key|
        choices("Type", "Choose a data type: ", ["string", "integer", "float", "boolean", "object", "array"]) { |o|
          if o == "string"
            addRow key, ""
          elsif o == "integer"
            addRow key, 0
          elsif o == "float"
            addRow key, 0.0
          elsif o == "boolean"
            addRow key, false
          elsif o == "array"
            addRow key, []
          elsif o == "object"
            addRow key, {}
          end
        }
      }
    }

    @save.signal_connect('clicked') {
      save
    }
    @window.signal_connect('destroy') { save }
    
    @window.show_all
  end
  def save
    out = {}
      @linesc.each { |k, v|
        out[v[:entry_key].text] = v[:value]
      }
      @out = out
      if @callback
        @callback.call out
      end
  end
  def addRow(key, value=nil)
    @linesc[key] = {type: value.class, container: Gtk::HBox.new, value: value}
    @linesc[key][:entry_key] = Gtk::Entry.new
    @linesc[key][:button] = Gtk::Button.new "Open"
    @linesc[key][:del] = Gtk::Button.new "-"
    @lines.add @linesc[key][:container]
    @linesc[key][:container].add @linesc[key][:entry_key]
    @linesc[key][:container].add @linesc[key][:button]
    @linesc[key][:container].add @linesc[key][:del]
    @linesc[key][:entry_key].text = key
    
    @linesc[key][:button].signal_connect('clicked') {
      
      if @linesc[key][:value].class == String
        openPromptDefault("Value", "Enter value for #{key}: ", @linesc[key][:value]) { |v| 
          @linesc[key][:value] = v
        }
      elsif @linesc[key][:value].class == Integer
        openPromptDefault("Value", "Enter value for #{key}: ", @linesc[key][:value].to_s) { |v| 
          @linesc[key][:value] = v.to_i
        }
      elsif @linesc[key][:value].class == Float
        openPromptDefault("Value", "Enter value for #{key}: ", @linesc[key][:value].to_s) { |v| 
          @linesc[key][:value] = v.to_f
        }
      elsif @linesc[key][:value].class == Hash
        sub = HashEditor.new(@linesc[key][:value])
        sub.callback { |v| 
          @linesc[key][:value] = v
        }
      elsif @linesc[key][:value].class == Array
        sub = ArrayEditor.new(@linesc[key][:value])
        sub.callback { |v| 
          @linesc[key][:value] = v
        }
      elsif @linesc[key][:value].class == TrueClass or @linesc[key][:value].class == FalseClass
        ask("Value", "Enter value for #{key}: ") { |v| 
          @linesc[key][:value] = v
        }
      else
        puts "Type #{@linesc[key][:value].class} not supported."
      end
    }
    
    @linesc[key][:del].signal_connect('clicked') {
      @linesc[key][:container].destroy
      @linesc.delete key
    }
    
    @linesc[key][:container].show_all
  end

  def removeRow(key)
  end
end

class ArrayEditor
  def callback(&fn)
    @callback = fn
  end
  
  def initialize(data, title = "Editor")
    @id = 0
    @data = data
    @window = Gtk::Window.new
    @window.title = title
    @lines = Gtk::VBox.new
    @controls = Gtk::HBox.new
    @add = Gtk::Button.new "+"
    @save = Gtk::Button.new "Save"
    @controls.add @add;@controls.add @save;
    @window.add @lines
    @lines.add @controls
    @linesc = {}
    
    data.each { |k|
      addRow k
    }

    @add.signal_connect('clicked') {
      choices("Type", "Choose a data type: ", ["string", "integer", "float", "boolean", "object", "array"]) { |o|
        if o == "string"
          addRow ""
        elsif o == "integer"
          addRow 0
        elsif o == "float"
          addRow 0.0
        elsif o == "boolean"
          addRow false
        elsif o == "array"
          addRow []
        elsif o == "object"
          addRow Hash.new
        end
        }
    }

    @save.signal_connect('clicked') {
      save
    }
    @window.signal_connect('destroy') { save }
    @window.show_all
  end

  def save
    out = []
    @linesc.each { |k, v|
      out.append v[:value]
    }
    @out = out
    if @callback
      @callback.call out
    end
  end
  
  def addRow(value)
    @id += 1
    key = @id.dup
    @linesc[key] = {type: value.class, container: Gtk::HBox.new, value: value}
    @linesc[key][:entry_key] = Gtk::Label.new
    @linesc[key][:button] = Gtk::Button.new "Open"
    @linesc[key][:del] = Gtk::Button.new "-"
    @lines.add @linesc[key][:container]
    @linesc[key][:container].add @linesc[key][:entry_key]
    @linesc[key][:container].add @linesc[key][:button]
    @linesc[key][:container].add @linesc[key][:del]

    if [String, Integer, Float, TrueClass, FalseClass].include? @linesc[key][:value].class
      @linesc[key][:entry_key].text = value.to_s
    elsif @linesc[key][:value].class == Array
      @linesc[key][:entry_key].text = "[##{value.length}]"
    elsif @linesc[key][:value].class == Hash
      @linesc[key][:entry_key].text = "{}"
    end
    @linesc[key][:button].signal_connect('clicked') {
      
      if @linesc[key][:value].class == String
        openPromptDefault("Value", "Enter value for #{key}: ", @linesc[key][:value]) { |v| 
          @linesc[key][:value] = v
          @linesc[key][:entry_key].text = v.to_s
        }
      elsif @linesc[key][:value].class == Integer
        openPromptDefault("Value", "Enter value for #{key}: ", @linesc[key][:value].to_s) { |v| 
          @linesc[key][:value] = v.to_i
          @linesc[key][:entry_key].text = v.to_s
        }
      elsif @linesc[key][:value].class == Float
        openPromptDefault("Value", "Enter value for #{key}: ", @linesc[key][:value].to_s) { |v| 
          @linesc[key][:value] = v.to_f
          @linesc[key][:entry_key].text = v.to_s
        }
      elsif @linesc[key][:value].class == Hash
        sub = HashEditor.new(@linesc[key][:value])
        sub.callback { |v| 
          @linesc[key][:value] = v
          @linesc[key][:entry_key].text = "{...}"
        }
      elsif @linesc[key][:value].class == Array
        sub = ArrayEditor.new(@linesc[key][:value])
        sub.callback { |v|
          @linesc[key][:value] = v
          @linesc[key][:entry_key].text = "[##{value.length}]"
        }
      elsif @linesc[key][:value].class == TrueClass or @linesc[key][:value].class == FalseClass
        ask("Value", "Enter value for #{key}: ") { |v| 
          @linesc[key][:value] = v
          @linesc[key][:entry_key].text = v.to_s
        }
      else
        puts "Type #{@linesc[key][:value].class} not supported."
      end
    }
    
    @linesc[key][:del].signal_connect('clicked') {
      @linesc[key][:container].destroy
      @linesc.delete key
    }
    @linesc[key][:container].show_all
  end

  def removeRow(key)
  end
end
